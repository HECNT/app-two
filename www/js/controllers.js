angular.module('app.controllers', [])

.controller('menuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('inicioCtrl', ['$scope', '$stateParams','$ionicPopup','$ionicPlatform', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$ionicPopup,$ionicPlatform) {

  $scope.btnGo = function(item) {
    console.log(item,'**********');
    location.href = '#/'+item
  }

  $ionicPlatform.ready(function() {
            if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    $ionicPopup.confirm({
                        title: "Internet Disconnected",
                        content: "The internet is disconnected on your device."
                    })
                    .then(function(result) {
                        if(!result) {
                            ionic.Platform.exitApp();
                        }
                    });
                }
            }
        });

}])

.controller('mapasCtrl', ['$scope', '$stateParams','$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $ionicLoading) {


  $scope.mapCreated = function(map) {
    console.log(map,'mapppp');
    $scope.map = map;
  };

  $scope.centerOnMe = function () {
    console.log("Centering");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
  };

}])

.controller('miPerfilCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('videosCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('loginCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

  console.log('Hola login');

}])

.controller('museosCtrl', ['$scope', '$stateParams','$http','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup) {

  $http.get('https://api-quinfort.herokuapp.com/where/get')
  .then(function(res){
    if (res.data.err) {
      $ionicPopup.alert({
       title: 'Error!',
       template: 'Hubo un error al tratar de conectarse al server'
     });
    } else {
      var d = res.data.result.rows
      var arr = []
      console.log(d);
      for (var key in d) {
        if (d[key].lugar_tipo_id == 2) {
          arr.push(d[key])
        }
      }
      $scope.listMuseo = arr;
      console.log(arr,'museo');
    }
  })

}])

.controller('plazasCtrl', ['$scope', '$stateParams','$http','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup) {

  $http.get('https://api-quinfort.herokuapp.com/where/get')
  .then(function(res){
    if (res.data.err) {
      $ionicPopup.alert({
       title: 'Error!',
       template: 'Hubo un error al tratar de conectarse al server'
     });
    } else {
      var d = res.data.result.rows
      var arr = []
      console.log(d);
      for (var key in d) {
        if (d[key].lugar_tipo_id == 6) {
          arr.push(d[key])
        }
      }
      $scope.listPlaza = arr;
      console.log(arr,'plaza');
    }
  })

}])

.controller('universidadesCtrl', ['$scope', '$stateParams','$http','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup) {

  $http.get('https://api-quinfort.herokuapp.com/where/get')
  .then(function(res){
    if (res.data.err) {
      $ionicPopup.alert({
       title: 'Error!',
       template: 'Hubo un error al tratar de conectarse al server'
     });
    } else {
      var d = res.data.result.rows
      var arr = []
      console.log(d);
      for (var key in d) {
        if (d[key].lugar_tipo_id == 5) {
          arr.push(d[key])
        }
      }
      $scope.listUni = arr;
      console.log(arr,'uni');
    }
  })

}])

.controller('parquesCtrl', ['$scope', '$stateParams','$http','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup) {

  $http.get('https://api-quinfort.herokuapp.com/where/get')
  .then(function(res){
    if (res.data.err) {
      $ionicPopup.alert({
       title: 'Error!',
       template: 'Hubo un error al tratar de conectarse al server'
     });
    } else {
      var d = res.data.result.rows
      var arr = []
      console.log(d);
      for (var key in d) {
        if (d[key].lugar_tipo_id == 1) {
          arr.push(d[key])
        }
      }
      $scope.listParque = arr;
      console.log(arr,'parque');
    }
  })

}])

.controller('gamingCtrl', ['$scope', '$stateParams','$http','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup) {

  $http.get('https://api-quinfort.herokuapp.com/where/get')
  .then(function(res){
    if (res.data.err) {
      $ionicPopup.alert({
       title: 'Error!',
       template: 'Hubo un error al tratar de conectarse al server'
     });
    } else {
      var d = res.data.result.rows
      var arr = []
      console.log(d);
      for (var key in d) {
        if (d[key].lugar_tipo_id == 4) {
          arr.push(d[key])
        }
      }
      $scope.listGame = arr;
      console.log(arr,'game');
    }
  })

}])

.controller('deportesCtrl', ['$scope', '$stateParams','$http','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup) {

  $http.get('https://api-quinfort.herokuapp.com/where/get')
  .then(function(res){
    if (res.data.err) {
      $ionicPopup.alert({
       title: 'Error!',
       template: 'Hubo un error al tratar de conectarse al server'
     });
    } else {
      var d = res.data.result.rows
      var arr = []
      console.log(d);
      for (var key in d) {
        if (d[key].lugar_tipo_id == 3) {
          arr.push(d[key])
        }
      }
      $scope.listDeporte = arr;
      console.log(arr,'deporte');
    }
  })

}])

.controller('galeriaCtrl', ['$scope', '$stateParams','$http','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http, $ionicPopup) {


}])
