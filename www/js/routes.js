angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('tabsController.inicio', {
    url: '/inicio',
    views: {
      'tab1': {
        templateUrl: 'templates/inicio.html',
        controller: 'inicioCtrl'
      }
    }
  })

  .state('tabsController.mapas', {
    url: '/mapa',
    views: {
      'tab2': {
        templateUrl: 'templates/mapas.html',
        controller: 'mapasCtrl'
      }
    }
  })

  .state('tabsController.miPerfil', {
    url: '/perfil',
    views: {
      'tab3': {
        templateUrl: 'templates/miPerfil.html',
        controller: 'miPerfilCtrl'
      }
    }
  })

  .state('tabsController.videos', {
    url: '/page5',
    views: {
      'tab4': {
        templateUrl: 'templates/videos.html',
        controller: 'videosCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('login', {
    url: '/page6',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('museos', {
    url: '/museo',
    templateUrl: 'templates/museos.html',
    controller: 'museosCtrl'
  })

  .state('plazas', {
    url: '/plaza',
    templateUrl: 'templates/plazas.html',
    controller: 'plazasCtrl'
  })

  .state('universidades', {
    url: '/uni',
    templateUrl: 'templates/universidades.html',
    controller: 'universidadesCtrl'
  })

  .state('parques', {
    url: '/parque',
    templateUrl: 'templates/parques.html',
    controller: 'parquesCtrl'
  })

  .state('gaming', {
    url: '/game',
    templateUrl: 'templates/gaming.html',
    controller: 'gamingCtrl'
  })

  .state('deportes', {
    url: '/deporte',
    templateUrl: 'templates/deportes.html',
    controller: 'deportesCtrl'
  })

  .state('galeria', {
    url: '/galeria',
    templateUrl: 'templates/galeria.html',
    controller: 'galeriaCtrl'
  })

$urlRouterProvider.otherwise('/page1/inicio')


});